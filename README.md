# grails-mongo

For test the API:

* POST
`curl -i -H "Content-Type:application/json" -X POST localhost:8080/products -d '{"name":"Orange","price":2.0}'`
* GET (List)
`curl -i -X GET localhost:8080/products`
* GET (Detail)
`curl -i -X GET localhost:8080/products/1`
* PUT
`curl -i -H "Content-Type:application/json" -X PUT localhost:8080/products/1 -d '{"price":3.0}'`
* DELETE
`curl -i -X DELETE localhost:8080/products/1`